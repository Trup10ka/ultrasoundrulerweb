assignEventListeners()

function assignEventListeners() {

    document.addEventListener("DOMContentLoaded", () => {

            const animatedElements = document.querySelectorAll('.info-container')

            animatedElements.forEach((animatedElement, index) => {

                    setTimeout(() => {
                            animatedElement.classList.add("animate-flyIn")
                        },
                        250 * (index * 2))

                    setUpEventListeners(animatedElement)
                }
            )
        }
    )
}

function setUpEventListeners(element)
{
    element.addEventListener("animationend", function animationendHandler(event) {
            if (event.animationName === "flyIn")
            {
                element.style.opacity = "1"
                element.classList.remove("animate-flyIn")
                element.removeEventListener("animationend", animationendHandler)
            }

            element.addEventListener("mouseenter", () => {
                    element.classList.remove("animate-flyDown")
                    element.classList.add("animate-flyUp")
                }
            )
            element.addEventListener("mouseleave", () => {
                    element.classList.remove("animate-flyUp")
                    element.classList.add("animate-flyDown")
                }
            )
        }
    )
}
